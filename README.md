# Docker

## Hosting

### Step 1 - Setup

Make sure the following dependencies are met:
- `git`
- `curl`
- `jq`

### Step 2 - Configure

Clone the repo
```sh
git clone https://codeberg.org/Hyperpipe/Docker
cd Docker
```

Make the setup script executable
```sh
chmod u+x gen.sh
```

Run the setup script
```sh
./gen.sh
```

#### Glossary

| Field | Value | Example |
| ----- | ----- | ------- |
| proxy | hostname of the [piped-proxy](https://github.com/TeamPiped/piped-proxy) you would like to connect to | piped-proxy.example.com |
| hyperpipe instance | hostname of the (to be setup) default hyperpipe instance | hyperpipeapi.example.com  |
| piped instance | hostname of the default piped instance | pipedapi.example.com |

### Step 3 - Docker

*Run the following commands as root*

Start the container
```sh
docker-compose up
```

## Submiting

If hosting a public instance, please  open a PR at [Hyperpipe/pages](https://codeberg.org/Hyperpipe/pages)

## Cleaning

***WARNING: The following command will remove ALL existing docker images that are not currently running***

*Run the following commands as root*

```sh
# docker system prune -a
# docker volume prune 
# docker network prune
# docker image prune
```