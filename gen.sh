#!/bin/sh

set -e

printf "\nEnter hostname for Proxy (Same as the one used for Piped): " && read -r proxy

printf '\nFetching List of Hostnames for Backends (Hyperpipe) \n\n'
curl -s "https://hyperpipe.codeberg.page/api/backend.json" | jq '.[].api_url|ltrimstr("https://")'

printf "\nEnter hostname for the default hyperpipe backend to be used: " && read -r backend

printf '\nFetching List of Hostnames for Backends (Piped) \n\n'
curl -s "https://piped-instances.kavin.rocks" | jq '.[].api_url|ltrimstr("https://")'

printf "\nEnter hostname for the default piped backend to be used: " && read -r piped

rm -f "*/docker-compose.yml"

cp docker-compose.template.yml docker-compose.yml

sed -i "s/PIPED_PROXY_URL/$proxy/g" docker-compose.yml

sed -i "s/PIPED_API_URL/$piped/g" docker-compose.yml

sed -i "s/HYPERPIPE_API_URL/$backend/g" docker-compose.yml

printf '\nDone!\n'
